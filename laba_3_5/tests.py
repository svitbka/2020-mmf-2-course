import test
from solve import findPolindrom

test.assert_equals(findPolindrom("abababa"), True)
test.assert_equals(findPolindrom("madam"),   True)
test.assert_equals(findPolindrom("level"),   True)
test.assert_equals(findPolindrom("Vitya"),   False)
test.assert_equals(findPolindrom("Sveta"),   False)
test.assert_equals(findPolindrom("Lera"),    False)

