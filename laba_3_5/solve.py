def findPolindrom(str, left = 0, right = 0):
    if (left == 0):
        right = len(str) - 1

    if (str[left] == str[right]):
        if (left == right or left > right):
            return True  
        else:      
            return findPolindrom(str, left + 1, right - 1)
    else:
        return False