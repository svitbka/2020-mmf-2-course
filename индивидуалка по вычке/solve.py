import math


def check_perfect(N):
    sum = 1

    for i in range(2, int(math.sqrt(N)) + 1):
        if (N % i == 0) :
            sum += i + N // i

    if (N == math.sqrt(N) ** 2):
        sum -= math.sqrt(N)
    if (sum == N):
        return True
    else: False
